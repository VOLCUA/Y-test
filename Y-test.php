<?php
/**
 * Plugin Name: Мій Плагін Y-test
 * Description: Додає блоки "Товари зі знижкою" та "Підписка на новини".
 * Version: 1.0
 * Author: VOLCUA
 * Author URI: https://volcua.com.ua
 */

if ( ! function_exists( 'register_block_type' ) ) {
    return;
}

//region newsletter

//region newsletter admin
function Y_test_add_admin_menu() {
    add_menu_page(
        'Підписки на розсилку', // Назва сторінки
        'VOLCUA Newsletter Subs', // Назва меню
        'manage_options', // Рівень доступу
        'my-gutenberg-plugin-newsletter-subs', // Slug сторінки
        'Y_test_newsletter_subs_page', // Функція відображення
        'dashicons-email-alt', // Іконка меню
        6 // Позиція у меню
    );
}
add_action('admin_menu', 'Y_test_add_admin_menu');
function Y_test_newsletter_create_table_sub() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'sub_newsletter';

    if ($wpdb->get_var("SHOW TABLES LIKE '{$table_name}'") != $table_name) {
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            email varchar(255) NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}
register_activation_hook(__FILE__, 'Y_test_newsletter_create_table_sub');

function Y_test_newsletter_subs_page() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'sub_newsletter';
    $subscribers = $wpdb->get_results("SELECT id, email FROM $table_name");

    echo '<div class="wrap"><h1>Підписники на розсилку</h1>';
    echo '<table class="wp-list-table widefat fixed striped">';
    echo '<thead><tr><th>ID</th><th>Email</th></tr></thead>';
    echo '<tbody>';
    foreach ($subscribers as $subscriber) {
        echo "<tr><td>{$subscriber->id}</td><td>{$subscriber->email}</td></tr>";
    }
    echo '</tbody></table></div>';
}
function Y_test_newsletter_process() {
    // Verify the nonce
    if ( ! isset( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'y_test_newsletter' ) ) {
        wp_die( 'Invalid nonce' );
    }

    // Get the submitted email
    $email = sanitize_email( $_POST['email'] );

    // Insert the email into the sub_newsletter table
    global $wpdb;
    $table_name = $wpdb->prefix . 'sub_newsletter';
    $wpdb->insert( $table_name, array( 'email' => $email ) );

    // Redirect back to the previous page
    wp_redirect( wp_get_referer() );
    exit;
}
add_action( 'admin_post_nopriv_y_test_process_newsletter', 'Y_test_newsletter_process' );
add_action( 'admin_post_y_test_process_newsletter', 'Y_test_newsletter_process' );
//endregion

function Y_test_newsletter_shortcode() {
    ob_start();
    ?>

    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
        <style>
            .y-test-newsletter
            {
                display: flex; max-width: 1000px; margin: auto;
                background: #FCF4F4;font-family: 'Poppins';
            }

            .y-test-newsletter-left
            {
                flex: 1;
                padding: 20px;
            }
            .y-test-newsletter-left-title
            {
                font-size: 54px;
                font-weight: 700;
            }
            .y-test-newsletter-left-subtitle
            {
                font-size: 16px;
            }

            .y-test-newsletter-right
            {
                flex: 1;
                padding: 20px;
            }

            .y-test-newsletter-right form
            {
                padding: 20px;
            }

            .y-test-newsletter-right
            {
                padding: 20px;
            }

            .y-test-newsletter-right input[type="email"],
            .y-test-newsletter-right input[type="checkbox"],
            .y-test-newsletter-right input[type="submit"] {
                margin-bottom: 10px;
            }

            .y-test-newsletter-right input[type="submit"] {
                background-color: #C2252C;
                color: #fff;
                font-size: 16px;
                padding: 20px 30px;
                border: none;
                cursor: pointer;

                box-shadow: 0 2px 4px #F3D3D5;
            }

            .y-test-newsletter-right input[type="submit"]:hover {
                background-color: #45a049;
            }
        </style>

    <noindex>
    <div class="y-test-newsletter">
        <div class="y-test-newsletter-left">
            <div class="y-test-newsletter-left-title">
                Subscribe to our newsletter
            </div><br>
            <div class="y-test-newsletter-left-subtitle">
                Subscribe today for thr latest Deals and More!
            </div>
        </div>
        <div class="y-test-newsletter-right">
            <form method="post" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>">
                <input type="hidden" name="action" value="y_test_process_newsletter">

                <label for="email">Email Address</label><br>
                <input type="email" id="email" name="email" placeholder="Ваша електронна адреса" required>
                <br>
                <input type="checkbox" id="agree" name="agree" required>
                <label for="agree">I agree to the terms and conditions</label>
                <br>
                <input type="submit" value="Subscribe">
                <?php wp_nonce_field( 'y_test_newsletter' ); ?>
            </form>
        </div>
    </div>
    </noindex>


    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'y_test_newsletter', 'Y_test_newsletter_shortcode' );

//endregion

//region discounts products

function y_test_products_discount_shortcode() {
    // Отримати товари зі знижкою
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => '_sale_price',
                'value' => 0,
                'compare' => '>',
                'type' => 'NUMERIC'
            )
        )
    );
    $query = new WP_Query($args);

    // Вивести картки товарів з прокруткою
    ob_start();
    if ($query->have_posts()) {
        echo '<div class="y-test-products-discount">';
        while ($query->have_posts()) {
            $query->the_post();
            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
            $regular_price = get_post_meta(get_the_ID(), '_regular_price', true);
            $sale_price = get_post_meta(get_the_ID(), '_sale_price', true);
            $short_description = get_post_meta(get_the_ID(), '_short_description', true);
            ?>
            <div class="product-card">
                <?php
                // Get the product image URL
                if ($image_url) {
                    echo '<img src="' . $image_url[0] . '" alt="Product Image">';
                }
                // Get the regular price

                echo '<p>Regular Price: ' . $regular_price . '</p>';
                // Get the sale price

                echo '<p>Sale Price: ' . $sale_price . '</p>';
                // Get the short description
                echo '<p>Short Description: ' . $short_description . '</p>';
                ?>
            </div>
            <?php
        }
        echo '</div>';
    } else {
        echo 'Товарів зі знижкою не знайдено.';
    }
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode('Y_test_products_discount', 'y_test_products_discount_shortcode');

//endregion